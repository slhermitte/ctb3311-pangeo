=====================
CTB3311 Pangeo  Examples
=====================

|Binder|

Examples of analysis of CMIP6 data using Pangeo.

Try these notebooks on pangeo.binder.io_ : |Binder|

See http://pangeo.io to learn more about the Pangeo project.


.. _pangeo.binder.io: http://binder.pangeo.io/

.. |Binder| image:: https://binder.pangeo.io/badge_logo.svg
 :target: https://binder.pangeo.io/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Fslhermitte%2Fctb3311-pangeo/master
